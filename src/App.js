import React, { useState } from 'react';
import Swal from 'sweetalert2';

const App = () => {
  const [formValues, setFormValues] = useState({
    nombre: '',
    app_paterno: '',
    app_materno: '',
    identificador: ''
  });

  const [formErrors, setFormErrors] = useState({
    nombre: '',
    app_paterno: '',
    app_materno: '',
    identificador: ''
  });

  const handleChange = (event) => {
    const { name, value } = event.target;
    setFormValues({ ...formValues, [name]: value });
  };

  const sendData = async () => {
    try {
      const response = await fetch('http://localhost:8080/person/insert', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(formValues)
      });
      
      if (response.ok) {
        Swal.fire({ 
          icon: 'success', 
          title: 'Envío exitoso', 
          showConfirmButton: false, 
          timer: 1500 
        }).then(() => {
          setFormValues({ nombre: '', app_paterno: '', app_materno: '', identificador: '' });
        });
      } else {
        throw new Error('Error al enviar los datos');
      }
    } catch (error) {
      console.error(error);
      Swal.fire({
        icon: 'error',
        title: 'Error al enviar los datos',
      });
    }
  };
  
  const sendEmail = (event) => {
    event.preventDefault();
    console.log('Enviando email');
  
    const errors = {};
    let hasErrors = false;
  
    // Validación de los campos del formulario
    Object.keys(formValues).forEach((key) => {
      if (key !== 'app_materno' && !formValues[key]) {
        errors[key] = 'Este campo es obligatorio.';
        hasErrors = true;
      } else {
        errors[key] = '';
      }
    });
  
    if (hasErrors) {
      setFormErrors(errors);
      console.log('Errores en el formulario:', errors);
    } else {
      setFormErrors({});
      console.log('Antes de sendData');
      sendData();
      console.log('Después de sendData');
    }
  };

  return (

    <div className="row valign-wrapper" style={{ height: '100vh' }}>
      <div className="col s12 m6 center-align" style={{ marginLeft: 'auto', marginRight: '0' }}>
          <img src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAABI1BMVEUSEhL////9/v8AAAAApf8SEhMAp/8TERIApP8TAAAQEBAPAAATEhEICAgRExIEqv8Vl9oQBwAcV3gQoe8RIzY9PT0RCA4LAAASDwcRZI+kpKQSEw+IiIgiIiLIyMg1NTVfX1/k5ORra2sqKira2tpJSUl5eXnu7u5XV1fs7Oy4uLjMzMwbGxuenp4VCwB+fn6xsbGRkZEVQ2IMAA4bcaEROlMOLUEQGioao+wah8YdgroQGSQRMkwVR2cYX4UVQlsMESQdfLIMLEMeltMUg8IbVX4OHTMfb6EceagaapMff7MaqvYHChgRIi4ZY5YOKEAykL4USnASOlgPGR0/gqoxcJMZOU0jT2g2k8UgbqYcLTkeeaILFiwWXYsaYH8rouE2dZaSR5MEAAAXIklEQVR4nO1dC1vaSNsOhmFIZpIQQBKMB0Stp5p6QDmJVqm1AvagXeu73e77/v9f8T3PJEBAsYddQ6/rm9vdCjHA3PMcZ+aZQVEkJCQkJCQkJCQkJCQkJCQkJCQkJCQkJCQkJCQkJCQkJCQkJCQkJCQkJCQkJP4/ggT/cIUqJj4CBBfJ4K9jt45ehvvhdRwemeJfeDnn/T/q8DNlUOSGLeKCGOW/Ahr0Cof34ERwFu+t678BQcXkHjYQfgM527YdVvh5MIcxm5sUOYImeKGIVWNBNabMDzteMU2gR23NqZ6eVBr1zM+j3qicnFYdzdZBU6lQC4C6szu3NWtMW4yeB5qpu8w4vczkLMtK/RrglbVMZf9A87nJ4T9QUXXuxc7O0uLKtPUU9IrbzmEF2KWSyeSv8YOXiZdatcqh41PwWcBwYXdjcW11YU6dHjkKRgOtKbD9eg4amUyjKFK5XwHKPokdZOUa+8wG/+UVSntrq3vb6pY6NSES8DAm4dphw0qlLSGBejF7eP4h/9M4OswW6zV4jzSSrB8yj3jmwu7a9vzc7BRlSNFatNdnXaFlqczVHXhE9xdhM+Ycvs9YKQvey3pXtUkB7LCkvlp8OT07ROfOPtWEBeU6h47mQuDAkEaIaJRpTnolhbiJHtNU+rdCxDFNF+z5LCfMstZinKh7u7tbK8YUPY3p9i6F76xV8sxHdiZEM0U4CvQVdAIESYo+k4s+gYgPERVe7Xkuy1dy2GXdSsH1VPXl9IwQ4PlVsECwm8a5xkUTodm6oQ6gf6d13NU0xpimaTZSJei4TM6OGlYa37XqiqRmOgCVotyrttEv1LIOpjOgs4JdaX53cRuwtVNeQZajL4SczMP0jlOXsevTb2dv3tTfnF1eHBeYDVIF9VVIwcnWUlY6lamCuPHaNECIR/x8BtyC1X7LTKF4wG9lb3UmEcH6bmmMI+VU8aB7wOIuM4P8AB7k6id5xwUtwByQva3j1Uzep2ZhojU/J8BiiP82YyWT1rsDF7NtSLHU8jaQmolAkNwbMSWKSZnJbk8yVhjoMQ6mRSjFSOhCb3kFah+868KfMlWXcjK5Hc8H4ESbbYhe1iW4PPQvurqxOsavz3J9Tx1GNBgoeXZB8ENiyXQ6TIOCcF8/ZtBhJgFrrED/pdpNePtpMAS/4DSgVdaV40L2Qbiqbj1GL+S4vDEQI/hP7aYN/imdjFBDgHNJpcGF9lyCebznFOEuq+NwbxoMFZMVITJbFcZhXEGourE+iV/AcW5A0WXFbiAw/D+NuZ4gKyiLxOGYgUMlngdSBIpFRnB0HJ8gOYYxk7vHkMhABxMckJvq/EQB9jlu68E4r9DrWMkhUoNfA2GmulkGneaZ1DkDU+we2jo88eKzRtODgO4dZKB7Mz2Xox9Qd5Df0xwTywtIkV9nrPSQIqah6eQoUsluEdNST9F7GfQ2BQXGZ2aMukq4R0BHU+ncsW1SE3zMzkNyD2WaWFaAonuUGwor/CV86pAhBvuig1mRZx/nhJ5imIxNTdFCTJqvgY6+dyAMEn1cRUUgDP8dub4KtsjZN6vPJ52ENNvK5Wo5a8TjpISi6qAdnL0Ha8/lIYTGGva5x87ggzMHYB6cOBtj/JbnyrOQ26xszC+OkEwkYKBHKGsMZGjV3lwc5nsH1Q/7xbo15AguNXfoEw8cDlpD6oyRGAnikMA/zyUta99G89DVUX5ba5CpGZBNivR0b3OE47wKqehhwAUyloses30wZe5CBndeyQ3dTjL1X3hvoOXvoxDPbRJj3AdfyirQsXVGxVzKVpTC9iywG0516qqxE6GYmFkyCCgAqmkq963AhpOmXOcaJNxDi0y9Y5iteQ6mbxXN47HlbibxeA+tcN9GJmpERxOJPXV8LKCra5uRHlhUuWmfdyHZy5xrUc1DGbnOSW7gf5Jd0FPI7+z9LgwWmzHm3yA4Pwv+L+NQsH5dXU0MCZYfGy6pRvSWDaAIlphqvPZpVO8ghwEHpu3nwAZDITYYxYTXQUvM+vExBPsXinNiwyNFLUdEWHp8sGqoy0OK2yqY8b5Vx3TdiwQASG0xY2U3fWNMpq1934RAb5/Ap9UdGl9SY7pVaEXuGgfyIyLcmzBhpBsrkRHVmkH5wce3NhCMNpqI+QBOtROr726AFmZPFD4uVXsdY/pN3ZaFfgYshxuzUelMHI2ra69ehHgFDE1+C0N6cyTV5GLNAn5EQi+iYqp25HsQkBiojNWy4/OlYlhjndjQ6dFsJjE7eX1hZGIj4DPhvSHIf8iJKUVU06INWbDnoJpWnOfg8ngjOPRp0jrEOVs6tLDE7pMzRj8620IIx1Ak8lVhfcQkPgRQq+7El7XRHtpF04XszVjpB7vEuAh1YyKeIIsTke554GxAWXNVDmHfbUJwyvVi86WcHoW+DYa9A08qcs4I9IXZSVh6YrUMB00e6giGjDTEXB88kOLUIX4excfQPbVEpuhRMjTDxM4oQ/VFYhI2n1BnDnan2MV+YgMhCTwQDhPT1qkem5raWTE7g4OMYcaWKI1KBhjOTGA485QQMWp4d/2BRqrCgBZhl+DaLuznpRWBVoS045uNq5bq9oDh2CImMpyAp5yuAG9a/SmNhvCgMOBKp4raszEa/3ztEhhe+DrmnMNkRf1xhmvfY6gNBhmQ3cMFG7KA1GV8DNklmMkYw8T6WEb6FMONH2EYuNOMYOhfxM/QuvD5L8vwewz1gGFKMET3IhhW4tTSIKVRollp4icYfs8OPZYL5jJQS/EC5qqpqxhleAL9W2SEwMhiMTGh2aMMEz/KkFPMavLd/nRNBzIZorAijmViY6jYkHhbFYaVPerugOGL0YHFw3g4ILi+MDkeUhjIU/iANI4S4ecK4yFxMBFu2bHFQ47zLDC0gBRLnR8w3BplaJS2F0ewPSn7iYJAGkGJhq5MMIQgaAJFRyTC8UV8D5QIRvi4gDucwgBnOhIRdXUMO5G+mCxDQonHC5lUMD5MWceuCYOqgwzOKMYnQmxBOnfEucJ1dWaQeZefKpjQ1cGyRuLVEzeaMEoEJe2Hw8wBSNB0j3LwkMY3TyOmWawWJP1m1NUsq5Nr7PSIOidWdBz7PjpzZuKC70FjsNZ2xnDdHEbcKasR3/gQnDk6U0hMPRJ1mSCbidoXFeGqSj3F9jgxlQejdlxKti8GqxpWy8c6PmGXRRabllIFR6TJDC7rEV1fH0aEhcejgD4yp5qYV03uZDFxf6h2OItRzQwm92s9nGzznEwy3T12n5tYpBmeg9OlNz7IQhnGi4lOUtfVV8ObZlTTo9e1Cw2Ggg+lAuTrg4lvCEngWakrOpTFOeetaBifznDhUjGWIqF8ESk+MmH6IhINd1RKIYLnWow+XGwBgh0r3RdhLu+iwWpnYtI7xuVDTt07cG6512hKUSHiKuhDMUa8DIrQ0HG1xUrlLpxxNTV1+3VjuLhoVTRqmor3GnI46859RKmfCdyjntOGhnzTsCJDL0SWtxObG+roNIyuGiPrGvMq5CxZCwsvKj1bp2IWEZeRiQkCvGsPVDSVyrzG9XxPrKa3mRefloI/9+xsF8t5IBxTMpKBJhK7C6ISShfrF4aqvtiMpGxoqoQeZESJiZX55NjQbhOLqSi3WbMSLEqJaf1UtwX5DISPai2VtrI24fFNCYOD4GL1uaJ50DRvZPEJl9dKg0RmdmdzdIixYmDmbqWDZXsrc5J3HKz5Ys7BaaVmDVZIUUcdr0AUrFZIY+Tvl7bHAexYULRksnbuonbpkZWXgOPM6u7O3s7c4vp4/VBJ1U03nxNL90FJcC7z5j/fisVLUVo6WCDFqVIDzBwki1OL3axNvfgYwmd5VMwKp9oOJsrUWBmvNBkfT4RXMSfgBUyj+wxRWVNWf407PbTCzDXWQolFIFy/wCm4GGu/wPi4ewNjuFRRE4mJOvtIMc34lUSwcmOfdvsTacHihJjcTqbTAwmKkpoPKDWMK1i0c+NCihhjtQnF0mAxw5fs3tgEwj5XlzYfUhxnOC8iCWdZUYyRSkbLL8STdFKUDwkJQrSEj7EPrX5dWYwVQwG4KKhJ1j74BHe7FNTVpykmZsphqCRsv2alkukRfkKaiDQ6oEbPJgSE5n+AuJvEAcYUYFL/GKufM1VfEaWl6s4TVVGJxOpKSBD8lH3diLiVIceg9itVKzqiUo66VdGJx/40CAJDj31CU2xXfcwwiamurU7gCJfnI4kAp7aTzaBviRZDCelhSWn9iLm4Hkz9ahsudT9p0ymgxfpIcAMgi8yR7YlxgqGWH+MIl+ZeRmdTcVcTaxZrlhWVY7AXxaq3mAuOjBDPPhLVq++dR0ZZcYBDMzwHk5BUbZ95oiZLN9TSYjRKiMfLe8ZIsgpdQzzOGcnWRZgYOlCrdnaDu7vw3alzilOK1qUDLmcqdhjQdEShvnVVAHeOPY2zM+XdQR30zObi/NKDQu9w5yGkadfZs0wNd0pZ3VytfrnvMBd34XCT+6LwEqtX43eiIwBFRdefah87Li7M4w4vIPlyZaNcLpdmXz5drQ8Zi2Y3j073W/vH+R7TfIqRFt7DZcd1LH7rwsB+6pu6nE+4/yOVq0AEozgkxl0kumGo8GN8d2kbq6epbfs+jo2wMNjDOnaWv8RpfRhDxreyPQkicUSHgBzzzDYJboURo4AfGQpwnCr0PBNeRk2wTYXiJrj8ZU24nfY5I9Opf44AwpbpOpWuiNS5RsvRbNf1wNdjFvAkgpdjbwRbaHA3Cndt5py+CZZlchXH5/HNIE4CbnGCLDywGiwVbZwcV2E8hAOiHwLD/8TwiTGneZxt1II0vFs/1ngwyf8bADy/02r3N1d2a/VG5bL4s6icNdq5IHhAV7VbLL417R+AyRXbbtWDLWfJcAfMTyMlBlIQIboQ9X0e44D3BwDBX/HZh/eZbpA9Qwr2Mwg2lQRBP9e+OmI2+B3zd2II3Y2lSy5z8tnLTK0LovjZXcBCirV65SLvsALlQ2f0ewBiGfdw2xnFfaC9o9NW9ufROjwiDrNd3PmOG9um70ajIGJSEMbFFF0rd3X752Fw3NRGBD88gOJ3EuFD/MLEX4xzhRISEhISEhISYplef3Bl2qcc/YsItoZEp5jCzSJG5AJg+IrBM/3x3Qg4aTW4XReTWPrwfr3/O3LtGWgNP159Wd7ZnZvfGMyBAr/Z+Z3dnVdLfY66XiqV1vqFXbq6USqJill9qRRgIzqDqhuzeGlQEyeeiuNo9IWSeKgrZXxD8e76Cl57xrNcDH0unOtdDheSdHVtO5z9XZwNLy3gs5VQbmoZ78Z6MNwsHGJ5b0ARi6UAg+p9dQ6ebeMkq1EK/qCr+AnruOkmePiMRyoZC+vDatFgD5D6KlJAGlSY6gvYC4v9HsA6hdUxhnDFCGvgwjLUwTFJgqE4naDPUMF9Ofh5YXeNl1v/i9CVzWhBLKreWJmsEGzAMKhU1NW9xKMMhz2wKp6uK0aUIS6HDxji7rEEbjoK5P1UZeM/JKiKRZflF7OzrzYTM2A6YBaCzNbGUmkr0e/ekKHYGBN0f4ThTLlceiU6KjAsdSNkvBdaomCIxe7qgOFAEQTTJ8pv/ymM2aCx4PlUdXFNNFp0+Ly4she2s88QFS/slCjDBXC7a8GrBCG8YSsx3CkUyjCxvqJGGJaFXJeE6jzfsV+BlgVOUkfHqeNeBGx+cGW5TyVkCI0LGjnCcAXtakZ0Rl/GM0L5wvrbPkN4zYBh2BGb24nEd7YA/iNgAWXQ1f2fsKnhji7RtHU9wnBbFawTUV86g6tRG30ZBkqwFWrfKMPE4tqQobEUXpx5vnPN9EDjlqNdaCzNiKaK6CWaOaOEDGeQWxk1d3GU4fz8fBBx0IXoyrrQO9EnoWEiw1URgRaHDEOPlXiqSPefQ90NhSQ+UvwjWhY6+uDPRl+GKKdNaP9mKfEwHgptU0LFX15aWxmGuYChOghLIcPA2Tynm+k3J9yPbqhBLFgOmq/3I/fQDoVOoxwfZxgUova1ONjHJ86dEwyXA2c0ZAi3vgg+/RkJhla3qWAS+nK5rA50Z07kpVsD4xIMF17OBLb4CMOZ7SAjClzkAEIZQoaDTKKf7AT3fm/T2z+CHjqB9Z1yGS0JLUI3hDatvii9EoF7fRgPF4I2zgq3ErHDtdm1FTWoPOlH+z7WlX4AgvuDKB8rw35wGGADs41SYuwS5naCoWj/nGqMMVzAcUlgzIZQxbLYGjxwr32G/VAaJ0OQmBLp9L3AksrDVHVdBGM9lCEMGha3XurjMlwZDpSEYi/jaj9kDCLgqfqAYV/CsTJEBzMfinF7LQyM6spuEP7W5xYCR9eXIbojXXmC4ctAbvqQQNnoa6nSt/uYGWK5zNqLvfnSyqCGC8e/G6/2XkVGjDqqXRBOkAduTw/Ge3h9OBkw+lydDe7TV/r3Q1dF/i7eZzaOc0zDMX5k1kLXxw651EeG8YNnuj42vB95PrxteFWPjufHX/1cCPO10V1q44emP7rn4mc/R0Li38DEA1h+7vbI336vRVKT4mFqpicq0Ux8iCcE4blBHp71yQmBwC62KHoUNw8Rj8MDihUNeGSfgi/HOjjK8TZictwDwLF0j+qcwtubLtauE5Pq+nTWh8l1wbObt7p98GcTD6xq4nmmQLF63WR4LJd3m7++vr51OWlizQF18licfttEJrTJOVz14OW8CXfBY+iTWyB4e+t5zet8/pbS6vU1sxXShLeZyvmlnH4+93qfb7X9j536lcMLHwu891n179qdxrumq/DC6edM/fOha2fbBa/gf/jYafxh2Pt/MFPR77469knHMb78wbSreqPxRQeC9smFbmT/ttm7RqPzRavedzqdo4J23+h8vItx6+EQhcOOc3LB3t7nmfM1axy0C27vXvVvGg67+lvDY2adkyzTvV7nP1mf9u6PmfMn9T9VNM453KR9q12xi0umfb3RmI3nXmtXFxq7uNJY51jTNPevj6rWesec+wONudMxUPbuW8exs1eMuscdp9c+MIEhvTlznKu/fUIU3Sme2NRuXfXuHf+mo5n6n7d+q8KI4h02QIbfGqetiqNVOl8rb9GQtff1SqV+ZbPO2WWlyZHh/jvG4OIfMe4AjoDT3n+PuZ+tOK69/9UptA/cwr1qH7fP2o0C7nYhztWFTZyPxWx73z5uOL76MQ8MNfAbNx1HK568br+/YqxycXfXI8RTtKvL48PLItM62bs7yl9nzhrtI8rub+7u3OkUD3HnvuqC+rX+Or+/cZ1OttnqgLAavdZnQ3xVDMrQPWy0st8+O07npPr2/i3o3et8E1VZKxbZXa3CtHefqvkmHgGpFS9s+wJluF9923Sr9399+Fj12P2H5nQ8jaJ4hT+ansnffm109jWFViuNd2+pe3TlOCenNta66dmWa/99aPvO/67d2ytwHwX78L7R+Fu7u3L8L1+onf3bZScNuGLg95tkv+jul6zNrjqNRpZV/3dgn4LfOUO/M5WvYeEmdzjxiOeAJzA54YzB8wJ1KLdFgwi1KecHLt4CAY6BQzGpDb8czz2gZsFWqM/AITG4RPGrggo2gavwRhrTHM4dSnTbo3gkvTGVWmj83hw8YRfPoxXnCptUEAZ9Czfs4rZXrLzHQA6pATzBXb0c99zjt7aY+J8nvlcIvz+A4NcseAR3zJomDY4ewGI5glv0pnSet9h7hbXnJm6aNzH9QF6DrZB4hBXmLWI/NB4lII714HhGL3QIFlhiDSIW64s9CATvA7Ym9zykiV2BvYAH9k/HDgkeqYC75kw8YZ/jQxQiNNUMG4Q7Q6iow/cwGcMnVNQc0qCWH18rRKggBxJ8L4YSVLVjv6COoGR/k2JvCQkJCQkJCQkJCQkJCQkJCQkJCQkJCQkJCQkJCQkJCQkJCQkJCQkJCQkJCQmJfwP/ByiMdUXp1XguAAAAAElFTkSuQmCC' alt="Logo" className="responsive-img left-align logo" />
      </div>
      <div className="col s12 center-align">
        <p className="flow-text">Registro de personas</p>
        <div className="container">
          <form className="col s12" onSubmit={sendEmail}>
            <div className="row">
              <div className="input-field col s12 m6 offset-m3">
                <input id="nombre" type="text" className="validate" required name="nombre" value={formValues.nombre} onChange={handleChange} />
                <label htmlFor="nombre">Nombre (s)</label>
                <span className="helper-text">{formErrors.nombre}</span>
              </div>
              <div className="input-field col s12 m6 offset-m3">
                <input id="app_paterno" type="text" className="validate" name="app_paterno" value={formValues.app_paterno} onChange={handleChange} />
                <label htmlFor="app_paterno">Apellido Paterno</label>
                <span className="helper-text">{formErrors.app_paterno}</span>
              </div>
              <div className="input-field col s12 m6 offset-m3">
                <input id="app_materno" type="text" className="validate" name="app_materno" value={formValues.app_materno} onChange={handleChange} />
                <label htmlFor="app_materno">Apellido Materno</label>
                <span className="helper-text">{formErrors.app_materno}</span>
              </div>
              <div className="input-field col s12 m6 offset-m3">
                <input id="identificador" type="text" className="validate" name="identificador" value={formValues.identificador} onChange={handleChange} />
                <label htmlFor="identificador">Identificador</label>
                <span className="helper-text">{formErrors.identificador}</span>
              </div>
              <div className="col s12 center-align">
                <button className="btn waves-effect waves-light" type="submit" name="action">Envíar</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  );
};

export default App;
